logbook (1.5.3-3) unstable; urgency=medium

  * Complete python2 removal, which was done already as a NMU but that the
    maintainer decided to ignore and perform partially; Closes: #936962

 -- Sandro Tosi <morph@debian.org>  Sat, 14 Mar 2020 16:33:37 -0400

logbook (1.5.3-2) unstable; urgency=medium

  * Remove python2 package.
  * Fix docs Index and Files reference on docs.
  * Bump standards version to 4.4.1
  * Bump debhelper and use debhelper-compat.

 -- Iñaki Malerba <inaki@malerba.space>  Sat, 21 Dec 2019 16:15:41 +0100

logbook (1.5.3-1) unstable; urgency=medium

  [ Iñaki Malerba ]
  * Update gitlab-ci.yml

  [ Downstreamer ]
  * New upstream version 1.5.3

 -- Iñaki Malerba <inaki@malerba.space>  Thu, 12 Dec 2019 22:14:20 +0100

logbook (1.4.3-1) unstable; urgency=medium

  [ Downstreamer ]
  * New upstream version 1.4.3

 -- Iñaki Malerba <inaki@malerba.space>  Tue, 22 Jan 2019 14:41:50 -0300

logbook (1.4.2-2) unstable; urgency=medium

  * New upstream version 1.4.2

 -- Agustin Henze <tin@debian.org>  Wed, 02 Jan 2019 18:26:48 +0000

logbook (1.4.1-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field

  [ Iñaki Malerba ]
  * New upstream version 1.4.0
  * New upstream version 1.4.1

 -- Iñaki Malerba <inaki@malerba.space>  Wed, 31 Oct 2018 21:09:33 -0300

logbook (1.3.0-3) unstable; urgency=medium

  [ Iñaki Malerba ]
  * Add Iñaki Malerba <inaki@malerba.space> as an uploader
  * Update Standards version. Deprecates x-python-version
  * debian/copyright: The files wildcard should be first
  * debian/rules: Remove get-orig-source target

  [ Agustin Henze ]
  * Set debhelper compatibility level to 11
  * [d/rules] Remove dpkg-parsechangelog invocation, use pkg-info.mk instead
  * Disable run unittest on building time, they are run by autopkgtests and
    remove unused build dependencies
  * Update doc paths according to dh compat 11
  * Add missing build-dependency on cython, python2.7-dev and python3-dev
  * Build cython module before calling to dh_auto_build
  * Change arch to any, shared lib is delivered
  * Enable hardening

 -- Agustin Henze <tin@debian.org>  Fri, 08 Jun 2018 15:40:21 -0300

logbook (1.3.0-2) unstable; urgency=medium

  [ Agustin Henze ]
  * Add gbp.conf

  [ Iñaki Malerba ]
  * debian/control: Change anonscm to salsa on vcs fields
  * Add needs-root requirement to autopkgtest (Closes: #900464)

 -- Iñaki Malerba <inaki@malerba.space>  Fri, 01 Jun 2018 14:01:04 -0300

logbook (1.3.0-1) unstable; urgency=medium

  [ Iñaki Malerba ]
  * New upstream version 1.3.0

 -- Inaki Malerba <inaki@malerba.space>  Wed, 04 Apr 2018 15:16:10 -0300

logbook (0.12.3-1) unstable; urgency=medium

  * Imported Upstream version 0.12.3
  * Add missing dependencies on `six` and `pytest` (Closes: #802404)
  * Build doc via `doc/Makefile`

 -- Agustin Henze <tin@debian.org>  Wed, 09 Dec 2015 11:52:57 -0300

logbook (0.10.0-1) unstable; urgency=medium

  * Imported Upstream version 0.10.0
  * Bumped Standard-Version to 3.9.6 (no changes required)

 -- Agustin Henze <tin@debian.org>  Fri, 21 Aug 2015 17:34:58 +0200

logbook (0.7.0-1) unstable; urgency=medium

  * Use uscan for get-orig-source target instead of wget
  * Imported Upstream version 0.7.0

 -- Agustin Henze <tin@debian.org>  Fri, 13 Jun 2014 20:39:04 -0300

logbook (0.6.0-1) unstable; urgency=low

  * Initial release (Closes: #596042).

 -- Agustin Henze <tin@debian.org>  Mon, 18 Nov 2013 20:01:13 -0300
